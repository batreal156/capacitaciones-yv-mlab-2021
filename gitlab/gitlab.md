# Menú
- [Información de como acceder](#Información-de-como-acceder)
- [Crear repositorios](#Crear-repositorios)
- [Crear grupos](#Crear-grupos)
- [Crear subgrupos](#Crear-subgrupos)
- [Crear issues](#Crear-issues)
- [Crear labels](#Crear-labels)
- [Roles que cumplen](#Roles-que-cumplen)
- [Agregar miembros](#Agregar-miembros)
- [Crear borads y manejo de boards](#Crear-borads-y-manejo-de-boards)

## Información de como acceder a git lab

1.-Se abre un navegador.
2.-Ingresamos a https://gitlab.com/.
3.-Iniciamos Sesion.
4.-Si no tiene una cuenta en GitLab, se crea una cuenta y se inicia el proceso. 


## Crear repositorios

1.- Primero ponemos en crear nuevo proyecto.

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/repositorio.jpeg">


2.- Ponemos el titulo de nuestro proyecto.

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/repositorio%202.jpeg">


3.-Por ultimo podemos poner en publico o privado, ponemos realizar repositorio con README y por ultimo ponemos en "crear proyecto". 

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/repositorio%203.jpeg">

4.- Y con eso finalizamos la creación de los repositorios. 

## Crear grupos

1.- Primero nos vamos a donde grupos y seleccionamos ahí. 

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/grupos%20(3).jpeg">

2.- Ponemos crear nombre del grupo.

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/grupos%20(4).jpeg">

3.-De igual manera podemos hacer el grupo publico o privado.

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/grupos%20(1).jpeg">

5.- Por ultimo invitamos a las miembros que deseamos y damos en crear grupo.


<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/grupos%20(2).jpeg">

## Crear subgrupos

Para crear un subgrupo, debe ser propietario o mantenedor del grupo, según la configuración del grupo.

De forma predeterminada, los grupos creados en:

GitLab 12.2 o posterior permite que tanto los propietarios como los encargados del mantenimiento creen subgrupos.
GitLab 12.1 o versiones anteriores solo permiten a los propietarios crear subgrupos.
La configuración se puede cambiar para cualquier grupo mediante:

Propietario de un grupo. Seleccione el grupo y vaya a Configuración> General> Permisos, LFS, 2FA .
Un administrador. Vaya a Área de administración> Descripción general> Grupos , seleccione el grupo y elija Editar .
Para obtener más información, consulte la tabla de permisos . Para obtener una lista de palabras que no pueden usarse como nombres de grupos, consulte los nombres reservados .

Los usuarios siempre pueden crear subgrupos si se agregan explícitamente como propietario (o mantenedor, si esa configuración está habilitada) a un grupo principal inmediato, incluso si un administrador deshabilita la creación de grupos en su configuración.

Para crear un subgrupo:

En el panel del grupo, haga clic en el botón Nuevo subgrupo .

Página de subgrupos

Crea un nuevo grupo como lo harías normalmente. Observe que el espacio de nombres del grupo principal inmediato se fija en Ruta del grupo . El nivel de visibilidad puede diferir del grupo principal inmediato.

Página de subgrupos

Haga clic en el botón Crear grupo para ser redirigido a la página del panel del nuevo grupo.

Siga el mismo proceso para crear grupos posteriores.

Afiliación


## Crear issues

1.- Nos dirijimos en la parte izquierda y nos vamos donde dice issues, creamos una nueva issues y listo, creamos el issue que deseamos.

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/issues%20(1).jpeg">

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/issues%20(2).jpeg">

## Crear labels

-Podemos hacerlo generando una etiqueta o TAG directamente desde gitlab.

El procedimiento es:
Acceder al repositorio.
Vamos donde dice Tags.
Crear una nueva Etiqueta con los datos completos que nos interesen y con eso estarian creadas para ponerlas en nuestra tabla.

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/labels.jpeg" width="1000">


## Roles que cumplen

- Acontinuación veremos en una imagen las funciones que cumplen: 

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/Funcion.jpeg">



## Agregar miembros

 El procedimiento es:
- Acceder al repositorio.
- Acceder a Repositorio -> Tags o Etiquetas. 
- Crear una nueva Etiqueta con los datos completos que nos interesen.



## Crear borads y manejo de boards

- Haga clic en el menú desplegable con el nombre del tablero actual en la esquina superior izquierda de la página Tableros de temas.
- Haz clic en Crear tablero nuevo .
- Ingrese el nombre de la nueva placa y seleccione su alcance: hito, etiquetas, cesionario o peso.
board
