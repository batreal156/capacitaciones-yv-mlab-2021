# Menú
- [Que es git](#Que-es-git)
- [Comandos de git en consola](#Comandos-de-git-en-consola)
- [Clientes git](#Clientes-git)
- [Clonación de proyecto por consola y por cliente](#Clonación-de-proyecto-por-consola-y-por-cliente)
- [Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)
- [Ramas de kraken](#Ramas-de-kraken)
- [Merge](#Merge)

## Que es git

Git es un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de aplicaciones cuando estas tienen un gran número de archivos de código fuente



## Comandos de git en consola
-Iniciar un repositorio vacío en unas carpeta específica.

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/git%20init.jpeg " width="1000">

-Añadir un archivo especifico.

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/git%20add%20nombre%20del%20archivo.jpeg" width="1000">

-Añadir todos los archivos del directorio

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/git%20add%20..jpeg" width="1000">

-Confirmar los cambios realizados. El “mensaje” generalmente se usa para asociar al commit una breve descripción de los cambios realizados.

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/git%20commit%20-m.jpeg " width="1000">


-Mostrar el estado actual de la rama(branch), como los cambios que hay sin hacer commit.

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/git%20status.jpeg" width="1000">



## Clientes git

-Un cliente GIT o software de control de versiones se usa mayormente para gestionar código fuente. Se diseñó para el mantenimiento de las versione de las aplicaciones cuando tienen un código fuente que contiene muchos archivos.


<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/cliente%20kraken.jpeg">

## Clonación de proyecto por consola y por cliente

1.- Clonación por gitlab

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/Clone%201.jpeg">


2.- También se puede clonar por el programa de Kraken. 

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/Clone%20kra.jpeg">

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/Clone%20kra2.jpeg">


## Commits por consola y por cliente kraken o smart

1.- Se puede realizar atravez de la consola bash. 

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/consola%20(1).jpeg">

2- Oh también atravez del programa Kraken. 

<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/consola%20(2).jpeg">

## Ramas de kraken

-Nos dirijimos al programa Kraken, nos vamos a donde dice Branch
y se nos crearia la primera rama (rama1).


<img src="https://gitlab.com/batreal156/Hola/-/raw/master/imagenes/rama1.jpeg">

## Merge

-La rama 1 se ubicamos en el master hasta que nos salga el marge into de la siguiente manera, luego se nos crea una integracion en el git kraken.









